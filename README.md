# flye Singularity container
### Bionformatics package flye<br>
Fast and accurate de novo assembler for single molecule sequencing reads<br>
flye Version: 2.7.1<br>
[https://github.com/fenderglass/Flye/]

Singularity container based on the recipe: Singularity.flye_v2.7.1

Package installation using Miniconda3-4.7.12<br>

Image singularity (V>=3.3) is automatically build and deployed (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

### build:
`sudo singularity build flye_v2.7.1.sif Singularity.flye_v2.7.1`

### Get image help
`singularity run-help ./flye_v2.7.1.sif`

#### Default runscript: STAR
#### Usage:
  `flye_v2.7.1.sif --help`<br>
    or:<br>
  `singularity exec flye_v2.7.1.sif flye --help`<br>


### Get image (singularity version >=3.3) with ORAS:<br>
`singularity pull flye_v2.7.1.sif oras://registry.forgemia.inra.fr/gafl/singularity/flye/flye:latest`


